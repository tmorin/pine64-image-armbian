#!/bin/bash

# arguments: $RELEASE $LINUXFAMILY $BOARD $BUILD_DESKTOP
#
# This is the image customization script

# NOTE: It is copied to /tmp directory inside the image
# and executed there inside chroot environment
# so don't reference any files that are not already installed

# NOTE: If you want to transfer files between chroot and host
# userpatches/overlay directory on host is bind-mounted to /tmp/overlay in chroot

RELEASE=$1
LINUXFAMILY=$2
BOARD=$3
BUILD_DESKTOP=$4

apt-get install --yes --no-install-suggests --no-install-recommends python avahi-daemon

if [ -f "/tmp/overlay/create_user_pine64" ]; then
    # create the pine64 user with the password disabled
    useradd -m -d /home/pine64 -s /bin/bash pine64

    # prepare the .ssh directory
    mkdir -p /home/pine64/.ssh
    touch /home/pine64/.ssh/authorized_keys
    chown -R pine64:pine64 /home/pine64
    chmod 700 /home/pine64/.ssh
    chmod 600 /home/pine64/.ssh/authorized_keys

    # add pine64 to sudoers people without password
    echo "pine64 ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/pine64
fi

if [ -f "/tmp/overlay/lock_root" ]; then
    # lock root
    usermod --lock root
    passwd -l root
    chage -d `date "+%F"` -E 2999-01-01 -I -1 -m 0 -M 999999 -W 31 root
fi

case ${BOARD} in
    pine64so)
        echo "fdtfile=allwinner/sun50i-a64-sopine-baseboard.dtb" >> /boot/armbianEnv.txt
        ;;
esac

sed -i -e 's/verbosity=.*/verbosity=7/g' /boot/armbianEnv.txt

if [ -f "/tmp/overlay/serial_only" ]; then
  echo "console=serial" >> /boot/armbianEnv.txt
fi
