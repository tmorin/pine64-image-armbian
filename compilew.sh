#!/usr/bin/env bash

COMMAND="default"
BUILD_DIRECTORY="/tmp/docker-image-armbian"
REPOSITORY="https://github.com/armbian/build"

BOARD="pine64so"
BRANCH="next"
RELEASE="stretch"
KERNEL_CONFIGURE="no"
KERNEL_ONLY="no"
BUILD_DESKTOP="no"
BUILD_MINIMAL="yes"
CLEAN_LEVEL=""
#CLEAN_LEVEL="make,debs,alldebs,cache,sources,extras"
NO_APT_CACHER="no"

CREATE_USER_PINE64="yes"
LOCK_ROOT="yes"
SERIAL_ONLY="no"

CUR_DIR=$(pwd)

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
case ${key} in
    default|docker|vagrant)
    COMMAND="$1"
    shift # past argument
    ;;
    -bd|--build-directory)
    BUILD_DIRECTORY="$2"
    shift # past argument
    shift # past value
    ;;
    -r|--repository)
    REPOSITORY="$2"
    shift # past argument
    shift # past value
    ;;
    -dcup|--disable-create-user-pine64)
    CREATE_USER_PINE64="no"
    shift # past argument
    ;;
    -dlr|--disable-lock-root)
    LOCK_ROOT="no"
    shift # past argument
    ;;
    -dso|--disable-serial-only)
    SERIAL_ONLY="no"
    shift # past argument
    ;;
    BOARD)
    BOARD="$2"
    shift # past argument
    shift # past value
    ;;
    BRANCH)
    BRANCH="$2"
    shift # past argument
    shift # past value
    ;;
    RELEASE)
    RELEASE="$2"
    shift # past argument
    shift # past value
    ;;
    KERNEL_CONFIGURE)
    KERNEL_CONFIGURE="$2"
    shift # past argument
    shift # past value
    ;;
    KERNEL_ONLY)
    KERNEL_ONLY="$2"
    shift # past argument
    shift # past value
    ;;
    BUILD_DESKTOP)
    BUILD_DESKTOP="$2"
    shift # past argument
    shift # past value
    ;;
    CLEAN_LEVEL)
    CLEAN_LEVEL="$2"
    shift # past argument
    shift # past value
    ;;
    NO_APT_CACHER)
    NO_APT_CACHER="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ ! -d "$BUILD_DIRECTORY" ]]; then
    mkdir -p ${BUILD_DIRECTORY}
    git clone --depth 1 ${REPOSITORY} ${BUILD_DIRECTORY}
fi

mkdir -p ${BUILD_DIRECTORY}/userpatches/overlay
mkdir -p ${BUILD_DIRECTORY}/userpatches/u-boot/u-boot-sunxi/board_pine64so

cp -f ${CUR_DIR}/customize-image.sh ${BUILD_DIRECTORY}/userpatches/customize-image.sh

rm -f ${BUILD_DIRECTORY}/userpatches/overlay/*
if [[ "$CREATE_USER_PINE64" == "yes" ]]; then
    touch ${BUILD_DIRECTORY}/userpatches/overlay/create_user_pine64
fi
if [[ "$LOCK_ROOT" == "yes" ]]; then
    touch ${BUILD_DIRECTORY}/userpatches/overlay/lock_root
fi
if [[ "$SERIAL_ONLY" == "yes" ]]; then
    touch ${BUILD_DIRECTORY}/userpatches/overlay/serial_only
fi

cd ${BUILD_DIRECTORY}

echo from $(pwd), execute: ./compile.sh ${COMMAND} \
    BOARD="${BOARD}" \
    BRANCH="${BRANCH}" \
    RELEASE="${RELEASE}" \
    KERNEL_CONFIGURE="${KERNEL_CONFIGURE}" \
    KERNEL_ONLY="${KERNEL_ONLY}" \
    BUILD_MINIMAL="${BUILD_MINIMAL}" \
    BUILD_DESKTOP="${BUILD_DESKTOP}" \
    CLEAN_LEVEL="${CLEAN_LEVEL}" \
    NO_APT_CACHER="${NO_APT_CACHER}" \
    $@

./compile.sh ${COMMAND} \
    BOARD="${BOARD}" \
    BRANCH="${BRANCH}" \
    RELEASE="${RELEASE}" \
    KERNEL_CONFIGURE="${KERNEL_CONFIGURE}" \
    KERNEL_ONLY="${KERNEL_ONLY}" \
    BUILD_MINIMAL="${BUILD_MINIMAL}" \
    BUILD_DESKTOP="${BUILD_DESKTOP}" \
    CLEAN_LEVEL="${CLEAN_LEVEL}" \
    NO_APT_CACHER="${NO_APT_CACHER}" \
    $@
