# pine64-image-armbian

The main purpose of this script is to built an image for [PINE A64-LTS](https://www.pine64.org/?page_id=46823) boards.
The script is just a wrapper of [Armbian Build Tool](https://docs.armbian.com/Developer-Guide_Build-Preparation/).
By default, the built image is dedicated to be a node of a [Docker Swarm based on PINE A64-LTS boards](https://gitlab.com/tmorin/pine64-docker-swarm).

The main tasks are:

- build armbian image
- fix the DTB path for the board pine64so
- increase verbosity of u-boot
- add user pine64 with sudo (NOPASSWD)
- lock root password
- force console to serial

The password of the user pine64 is disabled.
That means, the only way to login is from SSH using an authorized key.
By default there is no authorized key configured.

## Reference

```
./compilew.sh [commands] [options] [armbian build options]
```

**Commands:**

- `default`
- `docker`
- `vagrant`

**Options:**

- `-bd|--build-directory path/to/build/directory` default is /tmp/docker-image-armbian
- `-r|--repository URL of repository` default is /tmp/docker-image-armbian
- `-dcup|--disable-create-user-pine64` disable the creation of the user pine64
- `-dlr|--disable-lock-root` disable the lock of the user root
- `-dso|--disable-serial-only` disable the addition of `console=serial` in /boot/armbianEnv.txt

**Armbian build options**

List of available options: https://docs.armbian.com/Developer-Guide_Build-Options/

Some of them are already set by default:

```
BOARD="pine64so"
BRANCH="next"
RELEASE="buster"
KERNEL_CONFIGURE="no"
KERNEL_ONLY="no"
BUILD_DESKTOP="no"
BUILD_MINIMAL="yes"
CLEAN_LEVEL=""
NO_APT_CACHER="no"
```

**Examples**

Build an Ubuntu bionic image from a custom build directory
```bash
./compilew.sh -bd ~/tmp/bionic RELEASE="bionic"
```

Build an Ubuntu bionic image from a custom build directory with desktop and not user patch
```bash
./compilew.sh -bd ~/tmp/bionic --disable-lock-root --disable-create-user-pine64 RELEASE="bionic" BUILD_DESKTOP="yes"
```
