# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

# [1.1.0](https://gitlab.com/tmorin/pine64-image-armbian/compare/v1.0.0...v1.1.0) (2019-03-26)


### Bug Fixes

* improve armbianEnv.txt patch ([8a6db45](https://gitlab.com/tmorin/pine64-image-armbian/commit/8a6db45))
* the command was not took into account ([1839e86](https://gitlab.com/tmorin/pine64-image-armbian/commit/1839e86))
* the parsing of arguments is doing an infinite loop ([7f05544](https://gitlab.com/tmorin/pine64-image-armbian/commit/7f05544))
* the root account was expired ([dc520e8](https://gitlab.com/tmorin/pine64-image-armbian/commit/dc520e8))
* the root account was not enough locked (XD) ([745d702](https://gitlab.com/tmorin/pine64-image-armbian/commit/745d702))


### Features

* add arguments --repository and --disable-serial-only ([2f552ed](https://gitlab.com/tmorin/pine64-image-armbian/commit/2f552ed))
* add avahifor .local resolution ([7c30acc](https://gitlab.com/tmorin/pine64-image-armbian/commit/7c30acc))
* improve the command line with options ([5f170ed](https://gitlab.com/tmorin/pine64-image-armbian/commit/5f170ed))
* remove avahi packages ([cd49a77](https://gitlab.com/tmorin/pine64-image-armbian/commit/cd49a77))



<a name="1.0.1"></a>
## [1.0.1](https://gitlab.com/tmorin/pine64-image-armbian/compare/v1.0.0...v1.0.1) (2018-10-29)

### Bug Fixes

* the parsing of arguments is doing an infinite loop ([7f05544](https://gitlab.com/tmorin/pine64-image-armbian/commit/7f05544))

<a name="1.0.0"></a>
# 1.0.0 (2018-10-29)


### Features

* build an armbian image for Pine64-LTS ([8cef3bb](https://gitlab.com/tmorin/pine64-image-armbian/commit/8cef3bb))
